package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CacaNiquel {

    private Configuracao configuracao;
    private List<Slot> slots;

    public CacaNiquel() {

    }

    public List<Slot> carregarSlot(Configuracao configuracao){
        List<Slot> slots = new ArrayList<>();
        Slot slotAux = new Slot();
        slotAux.setFiguras(slotAux.carregarFiguras(configuracao.getQtdFigurasPorSlot()));

        for (int i = 0 ; i < configuracao.getQtdFigurasPorSlot(); i++) {
            Slot slot = new Slot();

            List<Figura> figuras = new ArrayList<>();
            for (Figura figuraAux : slotAux.getFiguras() ) {
                Figura figura = new Figura(figuraAux.getValor(), figuraAux.getDescricao());
                figuras.add(figura);
            }
            slot.setFiguras(figuras);
            slots.add(slot);
        }
        return slots;
    }

    public List<Slot> carregarSlot(Configuracao configuracao, boolean ePadrao){
        List<Slot> slots = new ArrayList<>();
        for (int i = 0; i < configuracao.getQtdSlots(); i++ ) {
            Slot slot = new Slot();
            slot.setFiguras(slot.carregarFiguras(configuracao.getQtdFigurasPorSlot(), ePadrao));
            slots.add(slot);
        }
        return slots;
    }

    public void embaralhar(List<Slot> slots) {
        for (Slot slot : slots ) {
            Collections.shuffle(slot.getFiguras());
        }
    }

    public void apurarResultado() throws InterruptedException {
        Figura primeiraFigura = new Figura();
        int pontuacao = 0;
        boolean ganhou = true;

        for (int i = 0; i < this.slots.size(); i++) {
            Figura figura = new Figura();
            figura = this.slots.get(i).getFiguras().get(0);
            pontuacao = pontuacao + figura.getValor();
            IO.imprimirFigura(figura);

            if (i == 0) {
                primeiraFigura = figura;
            }

            if (!primeiraFigura.equals(figura)) {
                ganhou = false;
            }
        }

        if (ganhou) {
            IO.imprimirPontuacao(pontuacao);
            IO.imprimirMensagemVitoria();
        } else {
            IO.imprimirMensagemDerrota();
        }

    }

    public Configuracao getConfiguracao() {
        return configuracao;
    }

    public void setConfiguracao(Configuracao configuracao) {
        this.configuracao = configuracao;
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

    public void copiarSlot(Slot slot, Configuracao configuracao) {
        List<Slot> slots = new ArrayList<>();
        for (int i = 0; i < configuracao.getQtdSlots(); i++) {
            slots.add(slot);
        }
        this.setSlots(slots);
    }
}
