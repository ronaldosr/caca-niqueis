package com.company;

public class Configuracao {

    private int qtdSlots;
    private int qtdFigurasPorSlot;
    private boolean ePadrao;

    public void configurarCacaNiquel() {
        this.qtdSlots = IO.lerDadosQtdSlots();
        this.qtdFigurasPorSlot = IO.lerDadosQtdFigurasPorSlot();
    }

    public void configurarCacaNiquel(int qtdSlots, int qtdFigurasPorSlot, boolean ePadrao) {
        this.setQtdSlots(qtdSlots);
        this.setQtdFigurasPorSlot(qtdFigurasPorSlot);
        this.setePadrao(ePadrao);

    }

    public Configuracao() {
    }

    public int getQtdSlots() {
        return qtdSlots;
    }

    public void setQtdSlots(int qtdSlots) {
        this.qtdSlots = qtdSlots;
    }

    public int getQtdFigurasPorSlot() {
        return qtdFigurasPorSlot;
    }

    public void setQtdFigurasPorSlot(int qtdFigurasPorSlot) {
        this.qtdFigurasPorSlot = qtdFigurasPorSlot;
    }

    public boolean isePadrao() {
        return ePadrao;
    }

    public void setePadrao(boolean ePadrao) {
        this.ePadrao = ePadrao;
    }
}
