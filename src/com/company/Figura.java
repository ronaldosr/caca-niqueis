package com.company;

public class Figura {

    private int valor;
    private String descricao;

    public Figura(int valor, String descricao) {
        this.valor = valor;
        this.descricao = descricao;
    }

    public Figura() {
    }

    public int getValor() {
        return valor;
    }

    public String getDescricao() {
        return descricao;
    }

}
