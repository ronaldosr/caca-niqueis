package com.company;

import java.net.SecureCacheResponse;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class IO {
    public static void imprimirTituloJogo() {
        System.out.println("*** Caça-Níquel *** \n");
    }

    public static String lerOpcaoMenuPrincipal() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Menu Principal \nSelecione uma opção \n(1) - Jogar \n(2) - Configurações \n(3) - Sair");
        return scanner.nextLine();
    }

    public static void imprimirMenuConfiguracoes() {
        System.out.println("Configurações");
    }

    public static int lerDadosQtdSlots() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe a quantidade de Slots no Caça-Níquel");
        return  scanner.nextInt();
    }

    public static int lerDadosQtdFigurasPorSlot() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe a quantidade de Figuras por Slot");
        return scanner.nextInt();
    }

    public static int lerPontuacaoFigura(int i) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe a pontuação para a " + i + "ª figura");
        return scanner.nextInt();
    }

    public static String lerDescricaoFigura(int i){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite a descrição para a " + i + "ª figura");
        return scanner.nextLine();
    }

    public static void imprimirInicioJogo() throws InterruptedException {
        int j = 5;
        System.out.println("Testando sua sorte  no Caça-Níquel!");

        for (int i = 1 ; i <= j; i++) {
            System.out.print("--");
            TimeUnit.SECONDS.sleep(1);
        }
        System.out.println("\n");
    }

    public static void imprimirFigura(Figura figura) {
        System.out.println(figura.getDescricao());
    }

    public static void imprimirPontuacao(int pontuacao) {
        System.out.println("Você fez " + pontuacao + " pontos.");
    }

    public static void imprimirMensagemVitoria() {
        System.out.println("-----> Parabéns, você ganhou!\n");
    }

    public static void imprimirMensagemDerrota() throws InterruptedException {
        System.out.println("\nInfelizmente, você perdeu!\n");
        TimeUnit.SECONDS.sleep(2);
    }

    public static String lerDadosJogarNovamente() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Pressione qualquer tecla para jogar novamente ou 'N' para voltar para o menu principal.");
        return scanner.nextLine();
    }

    public static void imprimirMensagemSaida() {
        System.out.println("Bye...");
    }

    public static void imprimirOpcaoInvalida() {
        System.out.println("Opção inválida!\n");
    }
}
