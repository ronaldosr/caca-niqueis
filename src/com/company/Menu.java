package com.company;

public class Menu {

    private boolean continua = true;

    public void setContinua(boolean continua) {
        this.continua = continua;
    }

    public Menu() throws InterruptedException {
        IO.imprimirTituloJogo();

        while (continua) {
            String opcao = IO.lerOpcaoMenuPrincipal();
            Configuracao configuracao = new Configuracao();

            if (opcao.equals("1")) {
                IO.imprimirInicioJogo();

                configuracao.configurarCacaNiquel(3, 4, true);
                CacaNiquel cacaNiquel = new CacaNiquel();
                cacaNiquel.setConfiguracao(configuracao);
                cacaNiquel.setSlots(cacaNiquel.carregarSlot(configuracao, true));

                cacaNiquel.embaralhar(cacaNiquel.getSlots());
                cacaNiquel.apurarResultado();

            } else if (opcao.equals("2")){
                IO.imprimirMenuConfiguracoes();

                configuracao.configurarCacaNiquel();
                CacaNiquel cacaNiquel = new CacaNiquel();
                cacaNiquel.setSlots(cacaNiquel.carregarSlot(configuracao));

                cacaNiquel.embaralhar(cacaNiquel.getSlots());
                IO.imprimirInicioJogo();
                cacaNiquel.apurarResultado();

            } else if (opcao.equals("3")){
                IO.imprimirMensagemSaida();
                setContinua(false);

            } else {
                IO.imprimirOpcaoInvalida();
            }
        }

    }
}
