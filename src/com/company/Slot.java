package com.company;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Slot {

    private List<Figura> figuras;

    public List<Figura> carregarFiguras(int qtdFigurasPorSlot) {
        List<Figura> figuras = new ArrayList<>();
        for (int i = 0; i < qtdFigurasPorSlot; i++){
            Figura figura = new Figura(IO.lerPontuacaoFigura(i+1), IO.lerDescricaoFigura(i+1));
            figuras.add(figura);
        }
        return figuras;
    }

    public List<Figura> carregarFiguras(int qtdFigurasPorSlot, boolean ePadrao) {
        if (ePadrao) {
            List<Figura> figuras = new ArrayList<>();
            for (int i = 0; i < qtdFigurasPorSlot; i++) {
                if (i == 0 ) {
                    Figura figura = new Figura(10, "banana");
                    figuras.add(figura);
                } else if (i == 1) {
                    Figura figura = new Figura(50, "framboesa");
                    figuras.add(figura);
                } else if (i == 2) {
                    Figura figura = new Figura(100, "moeda");
                    figuras.add(figura);
                } else {
                    Figura figura = new Figura(300, "sete");
                    figuras.add(figura);
                }
            }
            return figuras;
        }
        return null;
    }

    public Slot() {
    }

    public List<Figura> getFiguras() {
        return figuras;
    }

    public void setFiguras(List<Figura> figuras) {
        this.figuras = figuras;
    }

//    public List<Slot> preencherSlots(Slot slot, int qtdSlots) {
//        List<Slot> slots = new ArrayList<>();
//
//        for (int i = 0; i < qtdSlots; i++ ) {
//            slots.add(slot);
//        }
//        return slots;
//    }
}
